/* 
 *  Partida Estrela-Tri�ngulo
 *  Programador: Jos� W. R. Pereira
 *  Data: 2023-12-16
 *
 *  Objetivo
 *      Realizar a l�gica da sequ�ncia de acionamento de uma partida 
 *      estrela-tri�ngulo, conforme diagrama de simula��o.
 * 
 *  Entradas:
 *      S0: RC0 : Bot�o pulsador para desligar o motor acionado;
 *      S1: RC1 : Bot�o pulsador para ligar/acionar a partida;
 *  Sa�das:
 *      K1: RC2 : Contator comum do acionamento
 *      K2: RC3 : Contator da configura��o tri�ngulo
 *      K3: RC4 : Contator da configura��o estrela
 */
#define F_CPU   16000000L

#include <avr/io.h>
#include <util/delay.h>

int main(void) 
{   
            // Configura dire��o de dados para opera��o dos pinos
    DDRC |= (1<<DDC4) | (1<<DDC3) | (1<<DDC2);      //  Sa�das(set bit): K1, K2 e K3
    DDRC &= ~(1<<DDC1) & ~(1<<DDC0);                //  Entradas(clr bit): S0 e S1
    
    PORTC |= (1<<PORTC1);                           //  Habilita Pull-up Interno
    PORTC |= (1<<PORTC0);                           //  Habilita Pull-up Interno
    
    while (1) 
    {
//        LOGICA DE OPERA��O
//        
//        SE S1 for pressionado E motor desligado ENT�O
//            ligar estrela(K1 e K3)
//            temporizar 5 segundos
//            ligar tri�ngulo (K1 e K2)
//        
//        SE S0 for pressionado ENT�O
//            desligar motor (K1, K2 e K3)
//        
        
        if( !(PINC & (1<<PINC1) ) && !(PINC & (1<<PINC2)) )
        {
            PORTC |= (1<<PORTC2);
            PORTC |= (1<<PORTC4);
            _delay_ms(5000);
            PORTC &= ~(1<<PORTC4);
            PORTC |= (1<<PORTC3);
        }
        
        if( !(PINC & (1<<PINC0)) )
        {
            PORTC &= ~(1<<PORTC2);
            PORTC &= ~(1<<PORTC3);
            PORTC &= ~(1<<PORTC4);
        }
    }
}
