# Curso básico de microcontrolador AVR - Arduino Uno

Este é um curso que utiliza o Arduino Uno como plataforma para estudo do microcontroldor AVR. 
Como estrutura pedagógica, é utilizada uma abordagem baseada em projetos, pequenos e simples, para o estudo dos periféricos do microcontrolador e suas diversas formas de programação. 


|  Nº  | Projeto | Periférico | Linguagem | Observações |
|:----:|:--------:|:----------:|:---------:|:----------:|
|  1.0 | Sinalizador de garagem | Input(I), Output(O) | Assembly | Registradores de uso geral, loop, contagem, comparação |
|  1.1 |    | IO | C | Declaração de variáveis, defines, condicionais, loop, contagem, comparação |
|  1.2 |    | IO | C | Sub-rotinas |
|  2.0 | Partida Estrela-Triângulo| IO, Interrupt, Timer | C | Detecção de borda por software |
|  3.0 | Semáforo | PORT, IO, Timer | C | |
|  4.0 | Controlador de temperatura | LCD, AD, UART | C | LM35, TMP36, RS485-Modbus |
|  5.0 | Controlador de motor de passo | IO | C | Teclado matricial, L297/L298, DRV8825, UART-Bluetooth |
|  6.0 | Controlador de motor DC | IO, PWM | C | PID |
|  7.0 | Medidor de temperatura | UART, I2C, 1-Wire | C | LM75, DHT11/22, IoT** - ESP01 - MQTT |
