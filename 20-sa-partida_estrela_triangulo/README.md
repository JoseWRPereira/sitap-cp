
# Partida Estrela-Triângulo

# 1.0 - Objetivo e Requisitos

## 1.1 - Objetivo: 

Desenvolver uma aplicação, projeto eletrônico e programa, para um dispositivo de comando para partida Estrela-Triângulo. 

| Figura 1: Diagramas de comando e potência da partida Estrela-Triângulo |
|:-------------------:|
|![[img/partida-estrela-delta-comando-potencia.png]] |
|Fonte: [Wikipedia](https://pt.wikipedia.org/wiki/Partida_estrela-tri%C3%A2ngulo) |

## 1.2 - Requisitos da solução
1. Comportamento
    * Ao pressionar S1
	    * Ligar K1, K2 (fechamento estrela);
        * Temporizar 5 segundos
        * Desligar K2 e ligar K3 (fechamento triângulo).
	* Ao pressionar S0, em qualquer momento:
		* Desligar todos os contatores.
2. Estrutura (*Hardware*)
    * Acionamento via Relé ou transistor;
    * Tensão operação: 24V, 12V ou 5V.
  
## 2.0 - Planejamento da solução

Definir como será feita a entrega: simulador, montagem de protótipo, montagem de circuito em painel ou PCI, etc, assim como listar os materiais e ferramentas em função do tipo de entrega.
Por fim o planejamento do processo, em que deve ficar explicito como, utilizando os materiais listados e manipulando da ferramentas, chega-se ao produto final, com um encadeamento lógico das tarefas que compõem o processo.

## 2.1 - Planejamento do produto final

1. Projeto em plataforma de versionamento (Codeberg, GitLab, GitHub, etc);
2. Arquivo de simulação (SimulIDE);
3. Imagem da simulação como ilustração;
4. Diretório com o código em linguagem C (Arquivos fonte .c e de cabeçalho .h)
5. No arquivo README.md, descrição do problema e requisitos, bem como imagem de ilustração citada acima.


## 2.2 - Planejamento das ferramentas e materiais

### Ferramentas:

1. Simulador: SimulIDE (v0.4.15-SR10)
2. Ambiente de desenvolvimento: MPLabX (v6.05)
3. Compilador: XC8 (v2.36)
4. Alternativa: MPLAB Xpress

### Materiais

* Não há!

## 2.3 - Planejamento do processo
* [ ] Construir circuito do semáforo no simulador:
  * [ ] Montar dispositivos/circuitos de saída;
  * [ ] Montar dispositivos/circuitos de entrada;
  * [ ] Definir microcontrolador;
  * [ ] Fazer tabela de alocação de pinos;
  * [ ] Tirar um *print* do circuito final montado.
* [ ] Criar projeto do semáforo utilizando MPLabX e XC8
  * [ ] Novo projeto;
  * [ ] Arquivo principal;
  * [ ] Configurar e testar periféricos:
    * [ ] Configurar as saídas digitais;
    * [ ] Configurar as entradas digitais;
    * [ ] Integrar a rotina de atraso com as entradas e saídas(delay);
  * [ ] Montar uma lógica coerente com os requisitos do projeto.
* [ ] Criar projeto na plataforma de versionamento
  * [ ] Carregar arquivos conforme requisitos;
  * [ ] Escrever relatório.





## 3.0 - Solução
Produto ou processo que atinge o objetivo proposto, através da execução de seu planejamento e satisfação dos seus requisitos.




