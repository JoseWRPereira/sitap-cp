; ******************************************************************************
;	Profº José W. R. Pereira 				10/12/2023
;
;	Objetivo:
;		Sinalização de saída de garagem:
;		Piscar dois LEDs alternadamente, com intervalo de 500ms, quando 
;		o sensor de abertura do portão estiver acionado.
;			1: 500ms ligado
;			0: 500ms desligado
;
; 	Mapa de IOs
;
;	Pino Arduino 	| PORT.PIN	| Função
;	13		| PORTB.PB5	| LED (amarelo)
;	12		| PORTB.PB4	| LED (vermelho)
;	8		| PORTB.PB0	| Sensor de abertura do portão (Chave)
;
; ******************************************************************************

				; Inclusão das definições de pinos fornecidas pelo fabricante
.include "m328pdef.inc"

				; Programa alocado no enderaço 0 da memória de código.
.ORG 0X0000

				; PB5, PB4: out;    PB0: in;	0b00110000 = 0x30
	LDI	R16,0x30
	OUT 	DDRB,R16

DESLIGA_LEDS:
	LDI	R16,0x00
	OUT	PORTB,R16

INICIO:
	IN	R16,PINB
	SBRS	R16,0	
	RJMP	DESLIGA_LEDS
	
	RCALL	DELAY
	SBI	PORTB,PB5
	CBI	PORTB,PB4
	RCALL	DELAY
	CBI	PORTB,PB5
	SBI	PORTB,PB4
	RJMP	INICIO






DELAY:
	LDI	R16,0
	LDI	R17,0
	LDI	R18,41
DELAY_LOOP:
	INC	R16
	BRNE	DELAY_LOOP
	INC	R17
	BRNE	DELAY_LOOP
	DEC	R18
	BRNE	DELAY_LOOP
	RET

