# Projeto: [Sinalizador de garagem](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/10-sa-sinalizador_de_garagem)



# 1.0 - Objetivo e Requisitos
## 1.1 - Objetivo: Sinalizador de Garagem

Desenvolver uma aplicação, projeto eletrônico e programa, para um dispositivo de sinalização de segurança para saída de veículos em garagem. 


| Figura 1: Exemplo de sinalizador de garagem |
|:-------------------------------------------:|
| ![Exemplo](https://codeberg.org/JoseWRPereira/sitap-cp/raw/branch/main/10-sa-sinalizador_de_garagem/img/sinaleiro_garagem.jpg) |
| Fonte: [Loja Eletrônica Novo Mundo Condomínios](https://www.novomundocondominios.com.br/sinaleiro-para-garagem-pisca-pisca-modelo-toten/prod-4210868/)|


## 1.2 - Requisitos da solução
* Intervalo de alternância entre luzes: 1s;
* Luzes piscando enquanto o portão estiver em movimento ou aberto.

## 2.0 - Planejamento da solução

Definir como será feita a entrega: simulador, montagem em protótipo, montagem em painel ou placa de circuito impresso (PCI), etc, listar os materiais e ferramentas em função do tipo de entrega e explicitar como, utilizando os materiais listados e manipulando as ferramentas, chega-se ao produto final, com um encadeamento lógico das tarefas que integram o processo.

## 2.1 - Planejamento do produto final

* Arquivo de simulação;
* Imagem de circuito de simulação como ilustração;
* Montagem de protótipo.

## 2.2 - Planejamento das ferramentas e materiais

* Ferramentas:
    * Simulador: [SimulIDE](https://www.simulide.com/p/downloads.html) (v1.0.0 Stable Version)
    * Ambiente de desenvolvimento: [MPLabX](https://www.microchip.com/en-us/tools-resources/develop/mplab-x-ide)
    * Compilador: [XC8](https://www.microchip.com/en-us/tools-resources/develop/mplab-xc-compilers)
    * Alternativa: [MPLAB Xpress](https://www.microchip.com/en-us/tools-resources/develop/mplab-xpress)
    * Gravador: [AVRDUDESS](https://github.com/ZakKemble/AVRDUDESS)
* Materiais:
    * 01 - Arduino Uno + Cabo de conexão;
    * 01 - Matriz de contatos (*Protoboard*);
    * 02 - LEDs (Vermelho e Amarelo);
    * 02 - Resistores para os LEDs;
    * 01 - Botão ou chave de seleção;
    * 01 - Resistor de *pull-up*.


## 2.3 - Planejamento do processo

* [ ] Construir o circuito no simulador;
* [ ] Criar projeto de validação do circuito no simulador;
* [ ] Criar programa(firmware) da aplicação;
* [ ] Montar circuito em matriz de contatos;
* [ ] Testar programa(firmware) na montagem do protótipo;
* [ ] Registrar resultado da atividade.


## 3.0 - Solução

* [Assembly](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/11-cp-avr-atmega328p/01_5-asm-sinalizador_de_garagem)
* [ino](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/11-cp-avr-atmega328p/01_6-ino-sinalizador_de_garagem)
* [C](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/11-cp-avr-atmega328p/01_7-c-sinalizador_de_garagem)


| Figura 2: Simulação de operação do Sinalizador de Garagem |
|:-------------------------------------------------------:|
| ![Simula](https://codeberg.org/JoseWRPereira/sitap-cp/raw/branch/main/11-cp-avr-atmega328p/01_0-sim-sinalizador_de_garagem/sim01-sinalizador_de_garagem.gif) |
| Fonte: Autor |


~~~Assembly
.include "m328pdef.inc"

.ORG 0X0000

				; PB5, PB4: out;    PB0: in;	0b00110000 = 0x30
	LDI	R16,0x30
	OUT 	DDRB,R16

DESLIGA_LEDS:
	LDI	R16,0x00
	OUT	PORTB,R16

INICIO:
	IN	R16,PINB
	SBRS	R16,0	
	RJMP	DESLIGA_LEDS
	
	RCALL	DELAY
	SBI	PORTB,PB5
	CBI	PORTB,PB4
	RCALL	DELAY
	CBI	PORTB,PB5
	SBI	PORTB,PB4
	RJMP	INICIO

DELAY:
	LDI	R16,0
	LDI	R17,0
	LDI	R18,41
DELAY_LOOP:
	INC	R16
	BRNE	DELAY_LOOP
	INC	R17
	BRNE	DELAY_LOOP
	DEC	R18
	BRNE	DELAY_LOOP
	RET
~~~

~~~Arduino
void setup() 
{
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode( 8, INPUT);
}

void loop() 
{
  if( digitalRead(8) == 1 )
  {
    digitalWrite(12, HIGH);
    digitalWrite(13, LOW);
    delay(500);
    digitalWrite(12, LOW);
    digitalWrite(13, HIGH);
    delay(500);
  }
  else
  {
    digitalWrite(12, LOW);
    digitalWrite(13, LOW);
  }
}
~~~

~~~C
#define F_CPU 16000000L
#include <avr/io.h>
#include <util/delay.h>
int main(void) 
{   
    DDRB = 0b00110000;
    PORTB = 0;
    while (1) 
    {
        if( PINB & (1<<PINB0))
        {
            PORTB |= (1<<PORTB4);
            PORTB &= ~(1<<PORTB5);
            _delay_ms(500);
            PORTB ^= (1<<PORTB4 | 1<<PORTB5);
            _delay_ms(500);
        }
        else
        {
            PORTB &= ~(1<<PORTB4);
            PORTB &= ~(1<<PORTB5);
        }
    }
}
~~~

##


[Home](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main)
