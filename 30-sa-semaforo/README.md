# Projeto: Semáforo


# 1.0 - Objetivo e Requisitos

## 1.1 - Objetivo: Semáforo

Desenvolver uma aplicação, projeto eletrônico e programa, para um dispositivo de controle de tráfego (semáforo), a ser instalado em um cruzamento de pedestre. 

| Figura 1: Exemplo de semáforo |
|:-------------------:|
|![Semaforo](https://codeberg.org/JoseWRPereira/sitap-cp/raw/branch/main/30-sa-semaforo/img/semaforo_freepik.png)|
|Fonte: [freepik](https://br.freepik.com/vetores-premium/pedestre-atravessar-a-rua-com-pessoas-da-equipe-e-semaforo-e-cidade_5589436.htm) |

| Figura 2: Semáforo para veículos |
|:----------------------:|
| ![semaforogif](https://codeberg.org/JoseWRPereira/sitap-cp/raw/branch/main/30-sa-semaforo/img/semaforo.gif) |


## 1.2 - Requisitos da solução
1. Comportamento
    * Intervalo de tempo para o Semáforo de Veículos(sv):
        * Verde: 42s
        * Amarelo: 3s
        * Vermelho: 15s
    * Semáforo de Pedestre(sp) sincronizado com o de veículos:
        * Verde (sv) : Vermelho (sp)
        * Amarelo (sv) : Vermelho (sp)
        * Vermelho (sv) : Verde (sp)
        * Piscar Vermelho (sp) 3x antes de mudar em definitivo.
    * Botão pulsador para antecipar liberação de passagem dos pedestres:
        * Se faltar mais do que 10s para o acionamento da cor amarela, reduzir para 10s.
        * Se faltar menos do que 10s para o acionamento da cor amarela, manter o tempo restante.

2. Estrutra (*Hardware*)
    * Potência por sinaleiro: 7W ([PEA-3496](https://edisciplinas.usp.br/pluginfile.php/3465728/mod_resource/content/1/Iluminacao_Semaforos%202017.pdf))
    * Tensão operação: 24V

## 2.0 - Planejamento da solução

Definir como será feita a entrega: simulador, montagem de protótipo, montagem de circuito em painel ou PCI, etc, assim como listar os materiais e ferramentas em função do tipo de entrega.
Por fim o planejamento do processo, em que deve ficar explicito como, utilizando os materiais listados e manipulando da ferramentas, chega-se ao produto final, com um encadeamento lógico das tarefas que compoem o processo.

## 2.1 - Planejamento do produto final

1. Projeto em plataforma de versionamento (Codeberg, GitLab, GitHub, etc);
2. Arquivo de simulação (SimulIDE);
3. Imagem da simulação como ilustração;
4. Diretório com o código em linguagem C (Arquivos fonte .c e de cabeçalho .h)
5. No arquivo README.md, descrição do problema e requisitos, bem como imagem de ilustração citada acima.


## 2.2 - Planejamento das ferramentas e materiais

### Ferramentas:

1. Simulador: SimulIDE (v0.4.15-SR10)
2. Ambiente de desenvolvimento: MPLabX (v6.05)
3. Compilador: XC8 (v2.36)
4. Alternativa: MPLAB Xpress



### Materiais

* Não há!




## 2.3 - Planejamento do processo
* [ ] Construir circuito do semáforo no simulador:
  * [ ] Montar dispositivos/circuitos de saída;
  * [ ] Montar dispositivos/circuitos de entrada;
  * [ ] Definir microcontrolador (PIC16F887);
  * [ ] Fazer tabela de alocação de pinos;
  * [ ] Tirar um *print* do circuito final montado.
* [ ] Criar projeto do semáforo utilizando MPLabX e XC8
  * [ ] Novo projeto;
  * [ ] Arquivo principal;
  * [ ] Configurar e testar periféricos:
    * [ ] Configurar as saídas digitias;
    * [ ] Configurar as entradas digitais;
    * [ ] Integrar a rotina de atraso com as entradas e saídas(delay);
  * [ ] Montar uma lógica coerente com os requisitos do projeto.
    * [ ] Montar sequência de semáforo dos veículos;
    * [ ] Montar sequência do semáforo dos pedestres, de forma intertravada com o sv;
    * [ ] Incluir solicitação do pedestre;
    * [ ] Técnica de "não bloqueio" do código, por contador + condicional;
    * [ ] Incluir conceito básico de máquina de estados;
    * [ ] Modularizar o código.
* [ ] Criar projeto na plataforma de versinomento
  * [ ] Carregar arquivos conforme requisitos;
  * [ ] Escrever relatório.





## 3.0 - Solução
Produto ou processo que atinge o objetivo proposto, através da execução de seu planejamento e satisfação dos seus requisitos.


