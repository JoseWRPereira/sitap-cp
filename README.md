# Situações de aprendizagem - Controladores Programáveis

O objetivo deste projeto, é auxiliar o processo de ensino-aprendizagem utilizando de situações problema como meio condutor para o desenvolvimento de competências em ao menos um controlador programável, como microcontrolador AVR ou PIC. 

Os projetos aqui abordados são de simples aplicação, pensados para dar suporte às aulas de microcontroladores.




## Objetivo

* Desenvolver o curso com base na aprendizagem mediada por situações problema;

### Fundamentação/Filosofia de trabalho
* Aplicar conceitos de arquitetura de software/firmware com camadas de abstração;
  * Hardware > HAL > Aplicação > Usuário

* Aplicar o conceito de espiral do conhecimento no processo de aprendizado;


### Estrutura das atividades

* 1.0 - A Situação Problema
  * 1.1 - O contexto
  * 1.2 - Requisitos da solução
* 2.0 - Planejamento da solução
  * 2.1 - Planejamento do produto final
  * 2.2 - Planejamento das ferramentas e materiais
  * 2.3 - Planejamento do processo
* 3.0 - Solução
* 4.0 - Entrega




## Projetos

|  Nº  | Situação de Aprendizagem | Controlador Programável |
|:----:|:------------------------:|:-----------------------:|
|  1.0 | [Sinalizador de garagem](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/10-sa-sinalizador_de_garagem)| [AVR Atmega328P](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/11-cp-avr-atmega328p) |
|  2.0 | [Partida Estrela-Triângulo](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/20-sa-partida_estrela_triangulo)| [AVR_Atmega328P](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/21-cp-avr-atmega328p)|
|  3.0 | [Semáforo](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/30-sa-semaforo)   | [AVR Atmega328P](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main/31-cp-avr-atmega328p)|
|  4.0 | Controlador de temperatura              | |
|  5.0 | Controlador de motor de passo           | |
|  6.0 | Controlador de motor DC                 | |
|  7.0 | Medidor de temperatura                  | |

