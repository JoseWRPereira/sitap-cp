# Projeto: Sinalizador de garagem


# 1.0 - Objetivo e Requisitos
## 1.1 - Objetivo: Sinalizador de Garagem

Desenvolver uma aplicação, projeto eletrônico e programa, para um dispositivo de sinalização de segurança para saída de veículos em garagem. 


| Exemplo de sinalizador de garagem |
|:---------------------------------:|
| ![Exemplo](https://codeberg.org/JoseWRPereira/sitap-cp/raw/branch/main/10-sa-sinalizador_de_garagem/img/sinaleiro_garagem.jpg) |
| Fonte: [Loja Eletrônica Novo Mundo Condomínios](https://www.novomundocondominios.com.br/sinaleiro-para-garagem-pisca-pisca-modelo-toten/prod-4210868/)|


## 1.2 - Requisitos da solução
* Intervalo de alternância entre luzes: 1s;
* Luzes piscando enquanto o portão estiver em movimento ou aberto.

## 2.0 - Planejamento da solução

Definir como será feita a entrega: simulador, montagem em protótipo, montagem em painel ou placa de circuito impresso (PCI), etc, listar os materiais e ferramentas em função do tipo de entrega e explicitar como, utilizando os materiais listados e manipulando as ferramentas, chega-se ao produto final, com um encadeamento lógico das tarefas que integram o processo.

* 2.1 - Planejamento do produto final
* 2.2 - Planejamento das ferramentas e materiais
* 2.3 - Planejamento do processo


## 3.0 - Solução

Produto ou processo que atinge o objetivo proposto, através da execução do seu planejamento e satisfação dos seus requisitos.


##

[Home](https://codeberg.org/JoseWRPereira/sitap-cp/src/branch/main)
